﻿using UnityEngine;

[RequireComponent(typeof(Animator))]

public class IKController : MonoBehaviour
{
    public Transform[] gripPoints = new Transform[18];
    private Animator animator;

    public bool ikActive = false;
    public float isNearWall = 0;
    public Transform rightHand;
    public Transform leftHand;
    public Transform rightHandObj;
    public Transform leftHandObj;

    void Start()
    {
        animator = GetComponent<Animator>();
        UpdateAnimations();
    }

    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if (animator)
        {
            if (ikActive)
            {
                rightHandObj = FindNearestGripPoint(rightHand.position);
                leftHandObj = FindNearestGripPoint(leftHand.position);
                /*if (rightHandObj != null && Vector3.Distance(rightHand.position, rightHandObj.position) < 2)
                {
                    isNearWall = 1;
                    if (animator.GetCurrentAnimatorStateInfo(0).IsName("Vault"))
                    {
                        animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                        animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                        animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandObj.position);
                        animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandObj.rotation);
                    }
                    
                }
                else
                {
                    isNearWall = 0;
                }*/

                if (leftHandObj != null && Vector3.Distance(leftHand.position, leftHandObj.position) < 2 && animator.GetCurrentAnimatorStateInfo(0).IsName("Airborne"))
                {
                    isNearWall = 1;
                    if (animator.GetCurrentAnimatorStateInfo(0).IsName("Vault"))
                    {
                        animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                        animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                        animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandObj.position);
                        animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandObj.rotation);
                    }
                }
                else
                {
                    isNearWall = 0;
                }

            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else
            {
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                animator.SetLookAtWeight(0);
            }
        }
    }

    private Transform FindNearestGripPoint(Vector3 currentPosition)
    {
        Transform nearestGrip = gripPoints[0];
        float minDistance = Mathf.Infinity;
        foreach(Transform grip in gripPoints)
        {
            float distance = Vector3.Distance(grip.position, currentPosition);
            if(distance < minDistance)
            {
                nearestGrip = grip;
                minDistance = distance;
            }
        }
        return nearestGrip;
    }

    private void UpdateAnimations()
    {
        animator.SetFloat("IsNearWall", isNearWall);
    }
}

